ADB and Fastboot commands

Instructions for setting up Adb/Fastboot (windows)

1. On your phone go to Settings/Development and make sure USB debugging is ON and plug it in via the usb..  New phones go the settings about phone then click on the build number 10 times then back out to settings again and you will see developer options right above about phone. click on developer options then select usb debugging from there. also known as adb debugging.

2. Install drivers on your PC. Try pda.net for the drivers. Install the program by hitting “Run” and follow the instructions. You can remove it at a later time.

3. Download the file at the bottom of these instructions to your desktop and extract the contents. There is 1 folder and a install note (these instructions) that should extract. You can use a program called 7-ZIP to extract if needed (google it, its free and a great tool).

4. Open your “Start” menu and click on “My Computer”. Then choose “Local Disk C:”.(You can now click and drag the adb folder you extracted to the C: drive.

5.Open a command prompt and type cd\adb and hit enter. You should now see something that looks like this C:\adb>

6.Type adb devices then hit enter in the command prompt and it should list your device.

7. You can also type adb reboot bootloader which will put the device into bootloader mode. Now type fastboot devices then hit enter in the command prompt an it should list your device. (you must be in the bootloader for fastboot to recognize your device)

Screen shot of commands

You should now be ready to send commands to your phone via ADB or Fastboot. You will need to repeat steps 5-6 anytime you want to use ADB/Fastboot. To send a file using Adb or fastboot make sure to put the file in your “adb” folder before giving the command to send it to your device.

ADB FASTBOOT WINDOWS ZIP https://docs.google.com/file/d/0B_nizg6yUZ0NdmlqU1k0dXdHbGs/edit

SCREEN RECORD ON ANDROID 4.4 KITKAT
adb shell screenrecord /sdcard/filename.mp4
Save the file to the device SDcard and press ctrl+c to stop recording.
Here is a video on a easy way to screen record. VIDEO
 
Android SDK Tool, Has ADB included and much more http://developer.android.com/sdk/index.html

If using SDK the ADB file can be found here C:\Development\adt-bundle-windows-x86_64\adt-bundle-windows-x86_64\sdk\platform-tools

https://aiomobilestuff.com/oppo-preloader-driver-mtk-qualcomm-windows/

How to unlock your bootloader on the Nexus device using ADB/FASTBOOT

1.  Open a commandline window in your ADB folder by holding shift and right clicking mouse.
2.  Type adb devices then hit enter in the command prompt and it should list your device.
3.  Type adb reboot bootloader hit enter
4.  Type fastboot oem unlock hit enter
5,  Your tablet will then ask if you want to unlock.  Using the volume keys, highlight “Yes” and then press the power button to select it. Your bootloader will now be unlocked.  Choose to start your tablet and it will factory reset itself and reboot. You are now operating with an unlocked Nexus.
6.  Now that your nexus is unlocked you need to flash a recovery to it in ADB.
7.  Open a commandline window in your ADB folder by holding shift and right clicking mouse.
8.  Type adb devices then hit enter in the command prompt and it should list your device.
9.  Type adb reboot bootloader hit enter
10.  Type fastboot flash recovery (name of recovery.img) then hit enter. Make sure you put the recovery .IMG in the ADB folder on your computer.  I recommend twrp recovery.
 WHAT IS A LOGCAT
Logcat is the command to view the internal logs of the Android system. Viewing logs is often the best way to diagnose a problem, and is required for many issues. This way you’ll find out what apps are doing in the background without you noticing.

TAKING AN LOGCAT

Now type this in the terminal
adb devices (note) to make sure devices is connected
adb logcat -v long > logcat.txt
OR
adb logcat > logcat.txt:

you shall have a log file called name of problem.txt inside your ADB folder.  Now open the logcat.txt file and see whats your problem.

Here are some commands for Adb/Fastboot.
DO NOT USE ANY COMMANDS THAT YOU ARENT SURE OF OR DONT KNOW WHAT THEY DO. I AM NOT RESPONSABLE FOR ANYTHING THAT YOU DO TO YOUR PHONE.

ADB Commands

adb devices – list all connected devices
adb push <local> <remote> – copy file/dir to device
adb pull <remote> [<local>] – copy file/dir from device
adb sync [ <directory> ] – copy host->device only if changed

(-l means list but don’t copy)
(see ‘adb help all’)

adb shell – run remote shell interactively
adb shell <command> – run remote shell command
adb emu <command> – run emulator console command
adb logcat [ <filter-spec> ] – View device log
adb forward <local> <remote> – forward socket connections forward specs are one of: tcp:<port>
localabstract:<unix domain socket name>
localreserved:<unix domain socket name>
localfilesystem:<unix domain socket name>
dev:<character device name>
jdwp:<process pid> (remote only)
adb jdwp – list PIDs of processes hosting a JDWP transport
adb install [-l] [-r] [-s] <file> – push this package file to the device and install it
(‘-l’ means forward-lock the app)
(‘-r’ means reinstall the app, keeping its data)
(‘-s’ means install on SD card instead of internal storage)
adb uninstall [-k] <package> – remove this app package from the device
(‘-k’ means keep the data and cache directories)
adb bugreport – return all information from the device
that should be included in a bug report.
adb help – show this help message
adb version – show version num

DATAOPTS:

(no option) – don’t touch the data partition
-w – wipe the data partition
-d – flash the data partition
scripting:

adb wait-for-device – block until device is online
adb start-server – ensure that there is a server running
adb kill-server – kill the server if it is running
adb get-state – prints: offline | bootloader | device
adb get-serialno – prints: <serial-number>
adb status-window – continuously print device status for a specified device
adb remount – remounts the /system partition on the device read-write
adb reboot [bootloader|recovery] – reboots the device, optionally into the bootloader or recovery program
adb reboot-bootloader – reboots the device into the bootloader
adb root – restarts the adbd daemon with root permissions
adb usb – restarts the adbd daemon listening on USB
adb tcpip <port> – restarts the adbd daemon listening on TCP on the specified port

networking:

adb ppp <tty> [parameters] – Run PPP over USB.
Note: you should not automatically start a PPP connection.
< tty> refers to the tty for PPP stream. Eg. dev:/dev/omap_csmi_tty1
[parameters] – Eg. defaultroute debug dump local notty usepeerdns
adb sync notes: adb sync [ <directory> ]
< localdir> can be interpreted in several ways:
– If <directory> is not specified, both /system and /data partitions will be updated.
– If it is “system” or “data”, only the corresponding partition is updated.environmental variables:
ADB_TRACE – Print debug information. A comma separated list of the following values
1 or all, adb, sockets, packets, rwx, usb, sync, sysdeps, transport, jdwp
ANDROID_SERIAL – The serial number to connect to. -s takes priority over this if given.
ANDROID_LOG_TAGS – When used with the logcat option, only these debug tags are printed
.
https://aiomobilestuff.com/oppo-preloader-driver-mtk-qualcomm-windows/

Fastboot Commands

Quote: Example (fastboot flash recovery TWRP-recovery.img) then hit enter this will flash a recovery image to the device.
usage: fastboot [ <option> ] <command>
commands:
update <filename> reflash device from update.zip
flashall flash boot + recovery + system
flash <partition> [ <filename> ] write a file to a flash partition
erase <partition> erase a flash partition
getvar <variable> display a bootloader variable
boot <kernel> [ <ramdisk> ] download and boot kernel
flash:raw boot <kernel> [ <ramdisk> ] create bootimage and flash it
devices list all connected devices
reboot reboot device normally
reboot-bootloader reboot device into bootloader
options:
-w erase userdata and cache
-s <serial number> specify device serial number
-p <product> specify product name
-c <cmdline> override kernel commandline
-i <vendor id> specify a custom USB vendor id
-b <base_addr> specify a custom kernel base address
-n <page size> specify the nand page size. default: 2048
